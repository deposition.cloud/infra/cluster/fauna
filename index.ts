import * as pulumi from "@pulumi/pulumi"
import * as k8s from "@pulumi/kubernetes"

const http = 80
const https = 443

interface Features {
  postgres: boolean
  pgadmin: boolean
}

let config = new pulumi.Config()

const org = config.require("org")
const stack = pulumi.getStack()
const tier = pulumi.getProject()

const features = config.requireObject<Features>('features')

let domain = config.require('domain')
console.log(`Booting up ${domain}...`)

const aurora = new pulumi.StackReference(`${org}/aurora/${stack}`)

const deps = {
  aurora: aurora.requireOutput('default')
}

const out: any = {}

/*
* postgres and pgadmin
*/

if (features.postgres) {
  const postgresUsername = 'admin'

  const postgresShortName = 'pg'
  const postgresPrefix = 'postgres'
  const postgresNamespace = new k8s.core.v1.Namespace(postgresPrefix, {
    metadata: { labels: { name: postgresPrefix, stack, tier } }
  }, {})

  const postgresSecretNamePrefix = 'postgres'
  const postgresSecret = new k8s.core.v1.Secret(postgresSecretNamePrefix, {
    stringData: {
      'postgresql-password': config.requireSecret("postgresPassword"),
      'postgresql-postgres-password': config.requireSecret("postgresRootPassword"),
      'postgresql-replication-password': config.requireSecret("postgresReplicationPassword")
    },
    metadata: { namespace: postgresNamespace.metadata.name }
  }, {
    parent: postgresNamespace
  })

  const postgres = new k8s.helm.v3.Chart(postgresShortName, {
    repo: "bitnami",
    chart: "postgresql",
    values: {
      global: {
        storageClass: "openebs-jiva-csi-default",
        postgresql: {
          auth: {
            postgresPassword: "blah",
            username: "pgadminroot",
            password: "s3cret",
            database: "playtest"
          }
        }
      },
      volumePermissions: { enabled: true },
      readReplicas: {
        replicaCount: 2
      }
    },
    namespace: postgresNamespace.metadata.name
  })

  out.postgresHost = pulumi.interpolate`pg-postgresql.${postgresNamespace.metadata.name}.svc.cluster.local`
  
  if (features.pgadmin) {
    const pgadminShortName = 'pga'

    const pgadmin = new k8s.helm.v3.Chart(pgadminShortName, {
      repo: "runix",
      chart: "pgadmin4",
      values: {
        env: {
          email: config.requireSecret("adminEmail"),
          password: config.requireSecret("pgadminPassword"),
        }
      },
      namespace: postgresNamespace.metadata.name
    }, {
      parent: postgres
    })

    const pgadminHost = `postgres.${domain}`

    const pgadminIngress = new k8s.networking.v1beta1.Ingress(pgadminShortName, {
      metadata: {
        namespace: postgresNamespace.metadata.name,
        annotations: {
          'kubernetes.io/ingress.class': 'nginx',
          'cert-manager.io/cluster-issuer': deps.aurora.apply(o => o.out.clusterIssuer.metadata.name)
        }
      },
      spec: {
        tls: [{
          hosts: [pgadminHost],
          secretName: 'default-tls-secret'
        }],
        rules: [{
          host: pgadminHost,
          http: {
            paths: [{
              backend: { serviceName: `${pgadminShortName}-pgadmin4`, servicePort: 80 },
              path: '/',
              pathType: 'ImplementationSpecific'
            }]
          }
        }]
      }
    }, {
      dependsOn: [pgadmin]
    })
  }
}

export default {
  features,
  out,
  deps
}